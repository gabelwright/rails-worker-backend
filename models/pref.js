const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('pref', {
    postReplyEmail: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    commentReplyEmail: {
      type: Sequelize.BOOLEAN,
      defaultValue: true
    },
    primaryEmail: {
      type: Sequelize.STRING,
      allowNull: true,
      unique: true,
      validate: {
        isEmail: true,
      }
    }
  })
}
