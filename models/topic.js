const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('topic', {
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    about: {
      type: Sequelize.STRING,
      allowNull: false
    },
    location: {
      type: Sequelize.STRING,
      allowNull: false
    }
  })
}
