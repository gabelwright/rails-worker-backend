const Sequelize = require('sequelize');
const key_values = require('./../values');

const sequelize = new Sequelize(key_values.DB_CONNECTION, {
  host: 'localhost',
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

const User = sequelize.import(__dirname + "/user")
const Post = sequelize.import(__dirname + "/post")
const Topic = sequelize.import(__dirname + "/topic")
const Comment = sequelize.import(__dirname + "/comment")
const Pref = sequelize.import(__dirname + "/pref")
const Teacher = sequelize.import(__dirname + "/teacher")

User.hasMany(Post, {as: 'Posts'})
User.hasMany(Comment, {as: 'Comments'})
Topic.hasMany(Post, {as: 'Posts'})
Topic.hasMany(Comment, {as: 'TopicComments'})
Post.hasMany(Comment, {as: 'Comments'})
Comment.hasOne(Comment, {as: 'parent'})
Pref.hasOne(User)

sequelize.sync()

module.exports = {
    User,
    Post,
    Topic,
    Comment,
    Pref,
    Teacher
}
