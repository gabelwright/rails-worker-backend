const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('comment', {
   body: {
     type: Sequelize.TEXT,
     allowNull: false
   },
   poster: {
     type: Sequelize.STRING,
     allowNull: true
   },
   deleted: {
     type: Sequelize.BOOLEAN,
     defaultValue: false
   },
   newField: {
     type: Sequelize.BOOLEAN,
     defaultValue: true
   },
  })
  
}
