const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('post', {
    title: {
      type: Sequelize.STRING,
      allowNull: false
    },
    body: {
      type: Sequelize.TEXT,
      allowNull: false
    },
    comments: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    views: {
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    poster: {
      type: Sequelize.STRING,
      allowNull: true
    },
    deleted: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    }
  })
}
