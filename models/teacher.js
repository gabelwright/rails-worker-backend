const Sequelize = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  return sequelize.define('teacher', {
    email: {
      type: Sequelize.STRING,
      allowNull: false
    }
  }, {
    timestamps: true,
    updatedAt: false,
  })
}
