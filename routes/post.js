var express = require('express')
var app = express()
var auth = require('./../middlewares/auth')

const email = require('./../helpers/email');
let send_anon = email.anonPost

const models = require('./../models')
const Topic = models.Topic
const Post = models.Post
const Comment = models.Comment
const Pref = models.Pref
const User = models.User

app.get('/get_topics', (req, res) => {
  console.log(req.session.user)
  Topic.findAll().then(topics => {
    res.send({error: null, user: null, data: topics})
  })
})

app.post('/get', auth, (req, res) => {
  let limit = req.body.limit ? req.body.limit : 27
  let topicLocation = req.body.topicLocation
  if(topicLocation === 'recent'){
    Post.findAll({
      where: {deleted: false},
      order: [['createdAt', 'DESC']],
      limit: limit
    }).then(p => res.send({error: null, user: req.session.user, data: p}))
  }else{
    Topic.findOne({
      where: {location: topicLocation}
    }).then(t => {
      if(t){
        Post.findAll({
          where: {
            deleted: false,
            topicId: t.id,
          },
          order: [['createdAt', 'DESC']],
          limit: limit,
        }).then(p => res.send({error: null, user: req.session.user, data: p}))
      }else{
        res.send({error: 'Could not find topic', user: req.session.user, data: null})
      }
    })
  }

})

app.post('/get_from', auth, (req, res) => {
  let limit = req.body.limit ? req.body.limit : 18
  let {timestamp, topicLocation} = req.body
  if(!timestamp || !topicLocation)
    res.send({error: 'Missing timestamp and/or topic location values', user: req.session.user, data: null})
  if(topicLocation === 'recent'){
    Post.findAll({
      where: {
        deleted: false,
        createdAt: {[Op.lt]: timestamp},
      },
      order: [['createdAt', 'DESC']],
      limit: limit,
    }).then(p => res.send({error: null, user: req.session.user, data: p}))
  }else{
    Topic.findOne({
      where: {location: topicLocation}
    }).then(t => {
      if(t){
        Post.findAll({
          where: {
            deleted: false,
            topicId: t.id,
            createdAt: {[Op.lt]: timestamp},
          },
          order: [['createdAt', 'DESC']],
          limit: limit,
        }).then(p => res.send({error: null, user: req.session.user, data: p}))
      }else{
        res.send({error: 'Could not find topic', user: req.session.user, data: null})
      }
    })
  }

})

app.post('/get_single', auth, (req, res) => {
  Post.findOne({
    where: {
      id: req.body.postid,
      deleted: false
    }
  }).then(p => {
    Topic.findOne({
      where: {location: req.body.location}
    }).then(t => {
      if(p && t && t.id === p.topicId)
        res.send({error: null, user: req.session.user, data: p})
      else
        res.send({error: 'Post not found', user: req.session.user, data: null})
    })

  })
})

app.post('/add', auth, (req, res) => {
  let {title, body, topicId, userId, anon} = req.body
  let {first, last, id} = req.session.user
  if(userId !== id){
    res.send({error: 'Users do not match', user: req.session.user, data: null})
  }
  else if(!title || !body || !topicId || !userId){
    res.send({error: 'Missing params', user: req.session.user, data: null})
  }
  else{
    let poster = anon ? 'Coronado Teacher' : first + ' ' + last
    if(anon){
      send_anon(title, body, userId)
      userId = null
    }
    Post.create({title, body, poster, topicId, userId})
    .then(p => res.send({error: null, user: req.session.user, data: p}))
  }
})

app.post('/edit', auth, (req, res) => {
  let {body, postid} = req.body
  let {id} = req.session.user
  console.log(req.body)
  if(!body || !postid)
    res.send({error: 'Missing params', user: req.session.user, data: null})
  else{
    Post.findOne({
      where: {
        id: postid,
        deleted: false,
        userId: id
      }
    }).then(p => {
      if(p){
        p.update({
          body: body
        }).then(() => res.send({error: null, user: req.session.user, data: p}))
      }
      else
        res.send({error: 'Could not find post', user: req.session.user, data: null})
    })
  }
})

app.delete('/delete', auth, (req, res) => {
  let id = req.body.id
  if(!id)
    res.send({error: 'Post ID missing', user: req.session.user, data: null})
  else{
    let userId = req.session.user.id
    Post.findOne({
      where: {id: id}
    }).then(p => {
      if(p && p.userId === userId){
        p.update({
          deleted: true,
        }).then(() => {
          Comment.update(
            {deleted: true},
            {where: {postId: p.id}}
          )
        }).then(() => res.send({error: null, user: req.session.user, data: null}))
      }
      else if(p.userId !== userId){
        res.send({error: 'User does not have permission to delete post', user: req.session.user, data: null})
      }else{
        res.send({error: 'Could not find post with that ID', user: req.session.user, data: null})
      }
    })
  }
})

module.exports = app
