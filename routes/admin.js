var express = require('express')
var app = express()
var auth = require('./../middlewares/auth')
var getUser = require('./../middlewares/getUser')
var admin = require('./../middlewares/admin')
var faker = require('faker');

const email = require('./../helpers/email');
const list = require('./../helpers/teacherList');
let send_test = email.sendTestEmail
const master_password = '123456'

const models = require('./../models')
const Topic = models.Topic
const Post = models.Post
const Comment = models.Comment
const Pref = models.Pref
const User = models.User
const Teacher = models.Teacher

app.get('/get_users', admin, (req, res) => {
  User.findAll({
    order: [['id']],
  }).then(data => res.send({data, error: null, user: req.session.user}))
})

app.delete('/delete_post', admin, (req, res) => {
  let {id, password} = req.body
  console.log(id, password)
  if(!id || !password )
    res.send({error: 'Missing params', user: req.session.user, data: null})
  else if(password !== master_password)
    res.send({error: 'Incorrect password', user: req.session.user, data: null})
  else{
    Post.findOne({
      where: {id: id}
    }).then(p => {
      if(p){
        p.update({
          deleted: true,
        }).then(() => res.send({error: null, user: req.session.user, data: null}))
      }
      else{
        res.send({error: 'Could not find post with that ID', user: req.session.user, data: null})
      }
    })
  }

})

// app.post('/build_db', admin, (req, res) => {
//   // force: true will drop the table if it already exists
//   let db = req.body.db
//   if(db === 'comment'){
//     Comment.sync({force: true}).then(() => {
//       res.send('done')
//     })
//   }else if(db === 'post'){
//     Post.sync({force: true}).then(() => {
//       res.send('done')
//     })
//   }else if(db === 'topic'){
//     Topic.sync({force: true}).then(() => {
//       res.send('done')
//     })
//   }else if(db === 'user'){
//     User.sync({force: true}).then(() => {
//       res.send('done')
//     })
//   }else if(db === 'pref'){
//     Pref.sync({force: true}).then(() => {
//       res.send('done')
//     })
//   }else{
//     res.send('Could not find db')
//   }
//
// })

// app.post('/populate_users', admin, (req, res) => {
//   for(let i=0; i<req.body.limit;i++){
//     let first = faker.name.firstName()
//     let last = faker.name.lastName()
//     let username = faker.internet.userName()
//     let email = `${username}@episd.org`
//
//     bcrypt.hash('123456', saltRounds, (err, hash) => {
//       u1 = {first, last, username, email, password: hash}
//       User.create(u1).then(u => console.log('Created:', u.first, u.last))
//     })
//   }
//
//   if(req.body.includeGabe){
//     let first = 'Gabel'
//     let last = 'Wright'
//     let username = 'mattwri'
//     let email = `${username}@episd.org`
//
//     bcrypt.hash('123456', saltRounds, (err, hash) => {
//       u1 = {first, last, username, email, password: hash}
//       User.create(u1).then(u => console.log('Created:', u.first, u.last))
//       .then(res.send('done'))
//     })
//   }else{
//     res.send('done')
//   }
//
//
// })

// app.post('/populate_topics', admin, (req, res) => {
//   t0 = {title: 'General', about: 'General Stuff', location: 'general'}
//   t1 = {title: 'Science', about: 'Science Stuff', location: 'science'}
//   t2 = {title: 'Math', about: 'Math Stuff', location: 'math'}
//   t3 = {title: 'English', about: 'English Stuff', location: 'english'}
//   t4 = {title: 'Social Studies', about: 'Social Studies Stuff', location: 'socialstudies'}
//   t5 = {title: 'Arts', about: 'Arts Stuff', location: 'arts'}
//   t6 = {title: 'Languages', about: 'Languages Stuff', location: 'languages'}
//
//   Topic.bulkCreate([t0, t1, t2, t3, t4, t5, t6])
//   .then(() => res.send('done'))
// })

// app.post('/populate_posts', admin, (req, res) => {
//   let postArray = []
//   Topic.count().then(topicCount => {
//     User.findAll().then(users => {
//       let userCount = users.length
//       for(let i=0; i<req.body.limit;i++){
//         let title = faker.lorem.sentence()
//         let body = faker.lorem.paragraphs()
//         let userId = Math.floor(Math.random() * (userCount)) + 1;
//         let topicId = Math.floor(Math.random() * (topicCount)) + 1;
//         let u = users.find(item => item.id === userId)
//         let poster = u.first + ' ' + u.last
//         let today = new Date()
//         let createdAt = new Date(today.valueOf() - (Math.random() * 60000 * 60 * 24))
//         let p = {title, body, userId, topicId, poster, createdAt}
//         postArray.push(p)
//       }
//       Post.bulkCreate(postArray).then(() => res.send('done'))
//     })
//
//
//   })
//
// })

// app.post('/pop_comments', admin, (req, res) => {
//   for(let i=0; i<req.body.limit;i++){
//     let body = faker.lorem.paragraph()
//     let poster = faker.name.firstName() + ' ' + faker.name.lastName()
//     let userId = Math.floor(Math.random() * (10)) + 1;
//     let postId = req.body.postid;
//     let parentId = req.body.parentId
//
//     Comment.create({body, poster, userId, postId, parentId})
//     .then(p => console.log('Comment created'))
//   }
//   res.send('done')
// })

// app.post('/session', admin, (req, res) => {
//   req.session.user = 'it works?!?!';
//   req.session.loggedIn = true;
//   req.session.save()
//   console.log(req.session)
//   res.send({user: req.session.user, error: null})
// })

app.post('/test_email', admin, (req, res) => {
  let {email, data} = req.body
  send_test(email, data).then(data => {
    res.send(data)
  })
})

app.post('/add_topic', admin, (req, res) => {
  let {title, about, location} = req.body
  if(!title || !about || !location){
    res.send('Missing params')
  }
  else{
    Topic.create({title, about, location})
    .then(t => res.send(t))
  }
})

app.post('/add_teacher', admin, (req, res) => {
  if(req.body.type === 'bulk'){
    let bulk_object = []
    for(let i=0;i<list.list.length;i++){
      let item = {email: list.list[i]}
      bulk_object.push(item)
    }
    Teacher.sync({force: true}).then(() => {
      Teacher.bulkCreate(bulk_object)
      .then(() => {
        res.send('users created')
      })
    })
  }
  else if(req.body.type === 'single'){
    Teacher.create({
      email: req.body.email
    }).then(t => res.send(t))
  }
  else if(req.body.type === 'remove'){
    Teacher.findOne({
      where: {email: req.body.email}
    }).then(t => {
      t.destroy().then(() => res.send('Removed'))
    })
  }
  else{
    res.send('Missing params')
  }
})

module.exports = app
