var express = require('express')
var app = express()
var auth = require('./../middlewares/auth')
var getModUser = require('./../helpers/getModUser')
const bcrypt = require('bcrypt');
const saltRounds = 12;
var multer  = require('multer')
var upload = multer().none()
var urlCrypt = require('url-crypt')('knfvjhsdl87t6YGHK&^R%Tdklfjdslsdjivsdl,d^TYGVGfcasjckh,');

const email = require('./../helpers/email');
let send_test = email.sendTestEmail
let send_verify = email.sendVerificationEmail
const verifier = require('./../helpers/verify');
let generateToken = verifier.generateToken
let verifyToken = verifier.verifyToken

const models = require('./../models')
const Topic = models.Topic
const Post = models.Post
const Comment = models.Comment
const Pref = models.Pref
const User = models.User
const Teacher = models.Teacher

app.get('/get', auth, (req, res) => {
  let {id} = req.session.user
  User.findOne({
    where: {id: id}
  }).then(user => {
    let modUser = getModUser(user)
    res.send({error: null, user: modUser, data: null})
  })

})

app.post('/login', (req, res) => {
  const {username, password, remember} = req.body;
  if(!username || !password)
    res.send({user: null, error: 'Missing username/password'})
  else{
    user = User.findOne({where: {username: username}})
    .then(user => {
      if(user){
        bcrypt.compare(password, user.password, (err, result) => {
          if(result){
            let modUser = getModUser(user)
            req.session.user = modUser;
            // req.session.loggedIn = true;

            if(remember){
              req.session.cookie.maxAge = 3600000 * 24 * 14
            }
            req.session.save()
            res.send({user: modUser, error: null})
          }
          else{
            res.send({user: null, error: 'Invalid username/password'})
          }
        })
      }
      else{
        res.send({user: null, error: 'Invalid username/password'})
      }
    })
  }
})

app.post('/add', (req, res) => {
  let {first, last, username, email, password} = req.body;
  first = first.trim()
  last = last.trim()
  username = username.trim()
  email = email.trim()
  let hashed_password = ''

  Teacher.findOne({
    where: {email: email}
  }).then(t => {
    if(!t){
      res.send({user: null, error: 'That email address is not associated with a Coronado Teacher. BlueBoard is restricted to Coronado teachers. If you believe this is an error, please contact us at support@theblueboard.org', data: null})
    }
    else{
      User.findOne({
        where: {username: username}
      }).then(u => {
        if(u){
          res.send({user: null, error: 'Username/email is already in use', data: null})
        }
        else{
          bcrypt.hash(password, saltRounds, (err, hash) => {
            if(err){
              res.send({user: null, error: err, data: null})
            }
            else {
              Pref.create().then(p => {
                User.create({first, last, username, email, password: hash, prefId: p.id})
                  .then((u) => {
                    send_verify(first, last, email)
                    res.send({user: null, error: null, data: null})
                  })
              })

            }
          })
        }
      })
    }
  })


})

app.post('/update_password', auth, (req, res) => {
  let {oldPassword, newPassword} = req.body
  let {username} = req.session.user

  if(!oldPassword || !newPassword)
    res.send({error: 'Missing params', user: req.session.user, data: null})
  else if(newPassword.length < 6 || !/\d/.test(newPassword) )
    res.send({error: 'Password does not meet criteria', user: req.session.user, data: null})
  else{
    User.findOne({
      where: {username}
    })
    .then(user => {
      if(user){
        bcrypt.compare(oldPassword, user.password, (err, result) => {
          if(result){
            bcrypt.hash(newPassword, saltRounds, (err, hash) => {
              if(err){
                res.send({error: err, user: null, data: null})
              }
              else {
                user.update({password: hash})
                .then(() => res.send({error: null, user: req.session.user, data: null}))
              }
            })
          }
          else{
            res.send({error: 'Old password is invalid', user: req.session.user, data: null})
          }
        })
      }else {
        res.send({error: 'Invalid user', user: req.session.user, data: null})
      }

    })

  }
})

app.get('/profile', auth, (req, res) => {
  let user = req.session.user
  let cp = []

  Post.findAll({
    where: {
      userId: user.id,
      deleted: false
    },
    order: [['createdAt', 'DESC']]
  })
  .then(p => {
    Comment.findAll({
      where: {
        userId: user.id,
        deleted: false
      },
      order: [['createdAt', 'DESC']]
    })
    .then(c => {

      cp = c.map((item) => {

        return Post.findOne({
          where: {id: item.postId}
        })
        .then(post => {
          let {title} = post
          let {id, body, poster, createdAt, postId, topicId, parentId} = item
          let nc = {id, body, poster, createdAt, postId, topicId, parentId, title}
          return nc

        })
      })

      Promise.all(cp).then((values) => res.send({error: null, user: user, data: {posts: p, comments: values}}))

    })
  })
})

app.post('/get_token', auth, (req, res) => {
  let {email} = req.body

  if(email){
    let token = generateToken(email)
    res.send({error: null, user: req.session.user, data: token})
  }
})

app.post('/verify_email', (req, res) => {
  upload(req, res, function(err) {
    if (err) {
      if(err.code === "LIMIT_UNEXPECTED_FILE")
        res.send('For security reasons, emails with attachments are automatically rejected.')
      else
        res.send(err)
    }else{
      let {recipient, sender, from, subject} = req.body
      let bodyPlain = req.body['body-plain']
      let stripped = req.body['stripped-text'].replace(/ /g,'')
      let token = stripped.substring(stripped.lastIndexOf('(start)')+7, stripped.lastIndexOf('(end)')).replace(' ', '')

      let payload = verifyToken(token)

      res.status(200).send()

      if(payload && payload.email === sender){
        User.findOne({
          where: {email: payload.email}
        }).then(u => {
          if(u){
            u.update({confirmed: true})
          }
        })
      }else{
        console.log('payload', payload)
        console.log('sender', sender)
      }
    }
  })
})

app.post('/verify_token', (req, res) => {
  let {token} = req.body

  if(token){

    payload =  verifyToken(token)
    if(payload){
      User.findOne({
        where: {email: payload.email}
      })
      .then(u => {
        if(u){
          u.update({confirmed: true})
          .then(() => {
            req.session.user.confirmed = true
            res.send({error: null, user: req.session.user, data: 'Email Confirmed'})
          })
        }
        else{
          res.send({error: 'Could not find user', user: null, data: null})
        }

      })
    }else{
      res.send({error: 'Token is invalid, email could not be verified', user: null, data: null});
    }

  }else{
    res.send({error: 'Token is invalid, email could not be verified', user: null, data: null});
  }
})

app.post('/send_verify', (req, res) => {
  let {email, first, last} = req.body

  send_verify(first, last, email).then(result => {
    if(result.error)
      res.send({error: result.error, user: req.session.user, data: null})
    else
      res.send({error: null, user: req.session.user, data: result.data})
  })

})

app.post('/logout', (req, res) => {
  req.session.destroy()
  res.send({user: null, error: null, data: null})
})


module.exports = app
