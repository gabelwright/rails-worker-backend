var express = require('express')
var app = express()
var auth = require('./../middlewares/auth')

const models = require('./../models')
const Topic = models.Topic
const Post = models.Post
const Comment = models.Comment
const Pref = models.Pref
const User = models.User

app.use('/user', require('./user'))
app.use('/post', require('./post'))
app.use('/comment', require('./comment'))
app.use('/admin', require('./admin'))

app.get('/', (req, res) => {
  res.send('Server is running')
})

app.post('/', (req, res) => {
  res.send(req.body)
})

module.exports = app
