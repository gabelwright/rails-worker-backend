var express = require('express')
var app = express()
var auth = require('./../middlewares/auth')

const models = require('./../models')
const Topic = models.Topic
const Post = models.Post
const Comment = models.Comment
const Pref = models.Pref
const User = models.User

app.post('/get', auth, (req, res) => {
  let postid = req.body.postid
  Post.findOne({
    where: {
      id: postid,
      deleted: false
    }
  }).then(p => {
    if(!p || p.deleted)
      res.send({error: 'Could not find post', user: req.session.user, data: null})
    else{
      p.getComments().then(c => {
        res.send({error: null, user: req.session.user, data: c})
      })

    }
  })
})

app.post('/add', auth, (req, res) => {
  let {body, poster, userId, postid, parentId, topicId} = req.body
  Post.findOne({
    where: {id: postid}
  }).then(p => {
    let i = p.comments + 1
    p.update({
      comments: i
    })
    .then(() => {
      Comment.create({body, poster, userId: userId, postId: postid, parentId, topicId})
      .then(c => {
        res.send({error: null, user: req.session.user, data: c})
      })
    })
  })
})

app.delete('/delete', auth, (req, res) => {
  let id = req.body.id
  let userId = req.session.user.id
  Comment.findOne({
    where: {id: id}
  }).then(c => {
    if(c && c.userId === userId){
      c.update({
        body: '[deleted]',
        deleted: true,
      }).then(() => res.send({error: null, user: req.session.user, data: null}))
    }
    else if(c.userId !== userId){
      res.send({error: 'User does not have permission to delete comment', user: req.session.user, data: null})
    }else{
      res.send({error: 'Could not find comment with that ID', user: req.session.user, data: null})
    }
  })
})

module.exports = app
