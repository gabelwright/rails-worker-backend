const models = require('./../models')
const User = models.User

module.exports = function(req, res, next){
  if(req.session && req.session.user){
    User.findOne({
      where: {id: req.session.user.id}
    }).then(u => {
      if(u)
        req.session.fullUser = u
      return next();
    })
  }else{
    return res.send({error: 'No logged in user', user: null, data: null})
  }
}
