const verifier = require('./../helpers/verify');
let generateToken = verifier.generateToken

module.exports = function(req, res, next) {
  if (req.session && req.session.user){
    let user = req.session.user
    let modUser = {
      username: user.username,
      first: user.first,
      last: user.last,
      email: user.email,
      id: user.id,
      confirmed: user.confirmed,
      // loggedIn: true
    }

    if(!modUser.confirmed){
      modUser.token = generateToken(user.email)
    }
    req.session.user = modUser
    return next();
  }
  else if(req.session && !req.session.user){
    return res.send({error: 'No logged in user', user: null, data: null})
  }
  else{
    return res.send({error: 'No logged in user', user: null, data: null})
  }
};
