module.exports = function(req, res, next) {
  if (req.session && req.session.user){
    return next();
  }
  else if(req.session && !req.session.user){
    return res.send({error: 'No logged in user', user: null, data: null})
  }
  else{
    return res.send({error: 'No logged in user', user: null, data: null})
  }
};
