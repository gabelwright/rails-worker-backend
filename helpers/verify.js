const key_values = require('./../values');
var urlCrypt = require('url-crypt')(key_values.URLCRYPT_SALT);

exports.generateToken = function(email) {
  var payload = {
    email: email.toString(),
    date: new Date(),
  };
  var base64 = urlCrypt.cryptObj(payload);
  return base64
}

exports.verifyToken = function(token){
  let payload
  try {
    payload =  urlCrypt.decryptObj(token);

    return payload
  } catch(e) {
    console.log('Token did not verify')
    return null
  }
}
