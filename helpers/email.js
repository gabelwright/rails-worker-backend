var mailgun = require("mailgun-js");
const key_values = require('./../values');
const api_key = key_values.MAILGUN_KEY
const url = key_values.MAILGUN_URL
var urlCrypt = require('url-crypt')(key_values.URLCRYPT_SALT);

var mailgun = require('mailgun-js')({apiKey: api_key, domain: url});

exports.sendTestEmail = function(email, data=null) {
  var data = {
    from: 'Excited User <test@theblueboard.org>',
    to: email,
    subject: 'Test Email',
    text: data ? data : 'This is a test of BlueBoards email capabilities',
    'o:testmode': key_values.MAILGUN_TESTMODE
  };

  return mailgun.messages().send(data).then((body, error) => {
    return {body, error}
  })
}

exports.anonPost = function(title, post, userId){
  var data = {
    from: 'Anon <anon@theblueboard.org>',
    to: 'anon@theblueboard.org',
    subject: 'Anon Post',
    text: `Title: ${title} \nUser ID: ${userId} \nPost: \n${post}`,
    'o:testmode': key_values.MAILGUN_TESTMODE
  };

  return mailgun.messages().send(data).then((body, error) => {
    return {body, error}
  })
}

exports.sendVerificationEmail = function(first, last, email){
  var payload = {
    email: email.toString(),
    date: new Date(),
  };
  var base64 = urlCrypt.cryptObj(payload);

  const html = `<html><h1>Welcome to BlueBoard!</h1>Please verify your email address by visiting this <a href="${key_values.WEBSITE}/verify/${base64}">link</a></html>`

  var data = {
    from: 'BlueBoard Verification <verify@theblueboard.org>',
    to: `${first} ${last}, ${email}`,
    subject: 'Verify Email',
    text: `Please verify your email address by visiting this link: ${key_values.WEBSITE}/verify/${base64}`,
    html: html,
    'o:testmode': key_values.MAILGUN_TESTMODE
  };

  return mailgun.messages().send(data).then((body, error) => {
    console.log('error', error)
    console.log('body', body)
    if(error)
      return {data: null, error: error}
    else
      return {data: body, error: null}
  })

}
