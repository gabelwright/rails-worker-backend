const verifier = require('./../helpers/verify');
let generateToken = verifier.generateToken

module.exports = function(user) {
  if (user){
    let modUser = {
      username: user.username,
      first: user.first,
      last: user.last,
      email: user.email,
      id: user.id,
      confirmed: user.confirmed,
      // loggedIn: true
    }

    if(!modUser.confirmed){
      modUser.token = generateToken(user.email)
    }
    return modUser
  }
  else{
    return null
  }
};
