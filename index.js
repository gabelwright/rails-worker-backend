const express = require('express')
const bodyParser = require('body-parser')


const Sequelize = require('sequelize');
const session = require('express-session')
var SequelizeStore = require('connect-session-sequelize')(session.Store);
var app = express()
const key_values = require('./values');
const Op = Sequelize.Op
var auth = require('./middlewares/auth')

const sequelize = new Sequelize(key_values.DB_CONNECTION, {
  host: 'localhost',
  dialect: 'postgres',

  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
});

sequelize.authenticate()
  .then(() => {
    console.log('Connected to database');
  })
  .catch(err => {
    console.error('Unable to connect to the database');
});

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));

var myStore = new SequelizeStore({
  db: sequelize
})

app.use(session({
  store: myStore,
  name: 'bb',
  secret: key_values.SESSION_SALT,
  domain: key_values.WEBSITE,
  cookie: {
    httpOnly: false,
    saveUninitialized: true,
    secure: false,
    maxAge: null,
  },
  resave: true,
  saveUninitialized: false
}))

myStore.sync();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", key_values.WEBSITE);
  res.header('Access-Control-Allow-Credentials', true);
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'POST, GET, DELETE');
  next();
});

app.use(require('./routes'))

app.listen(key_values.PORT, () => console.log(`Listening on port ${key_values.PORT}`))
